import unittest

import numpy as np
from d3m.container.numpy import ndarray

#  from common_primitives.diagonal_mvn import DiagonalMVN, Hyperparams as MVNHyperparams, Params as MVNParams
#from common_primitives.hmm import HiddenMarkovModel, Hyperparams, Params
import random


@unittest.skip
class TestHMM(unittest.TestCase):
    def test_fit(self):
        # Calculate fit using the primitives
        pass
        #  regularizer = Dirichlet(hyperparams=DirichletHyperparams.defaults())
        #  regularizer.set_params(params=MVNParams(mean=ndarray([0.0, 0.0, 0.0], generate_metadata=True),
        #                                          covariance=ndarray(np.identity(3)*0.1, generate_metadata=True)))

        #  real_start = [0.6, 0.4]
        #  real_trans = [[0.7, 0.3], [0.4, 0.6]]
        #  real_emit = [[1.0, 0.0, 0.0], [0.1, 0.3, 0.6]]
        #  real_states=('healthy', 'fever')
        #  real_obs=('normal', 'cold', 'dizzy')
        #
        #  obs_data = []
        #  states_data = []
        #  for _ in range(2000):
        #      obs = []
        #      states = []
        #      prev = 0 if random.random() < 0.6 else 1
        #      for i in range(10):
        #          trans = real_trans[prev]
        #          curr = 0 if random.random() < trans[0] else 1
        #          states.append(curr)
        #          emit = real_emit[curr]
        #          a = random.random()
        #          if a < emit[0]:
        #              ob = 0
        #          elif a > 1 - emit[2]:
        #              ob = 2
        #          else:
        #              ob = 1
        #          obs.append(ob)
        #      obs_data.append([real_obs[n] for n in obs])
        #      states_data.append([real_states[n] for n in states])
        #
        #  obs_data = np.array(obs_data)
        #  states_data = np.array(states_data)
        #  #  print(obs_data[0])
        #  #  print(states_data[0])
        #
        #  hy = Hyperparams.defaults()
        #  hy1 = dict(hy)
        #  hy1['start_prior'] = ndarray([100,10], generate_metadata=True)
        #  #  hy1['transition_prior'] = ndarray([[40,20], [120, 80]], generate_metadata=True)
        #  #  hy1['emission_prior'] = ndarray([[100000,10,20], [3000, 1000, 2000]], generate_metadata=True)
        #  hy2 = Hyperparams(hy1)
        #  hmm = HiddenMarkovModel(hyperparams=hy2)
        #  hmm.set_params(params=Params(states=('healthy', 'fever'), observations=('normal', 'cold', 'dizzy')))
        #
        #  hmm.set_training_data(outputs=states_data, inputs=obs_data)
        #
        #  hmm.fit(iterations=500, fit_threshold=0, batch_size=None)
        #  # 300 is 0.12

        ###############


        #  w_primitive = hmm.get_params()['start_probability']
        #  w_primitive = hmm.get_params()['emission_probability']
        #  w_primitive = hmm.get_params()['transition_probability']

        # Calculate fit analytically assuming noise variance is correct
        #  train_x_with_ones = np.insert(train_x, 0, 1, axis=1)
        #  inv_covar = np.zeros([4, 4])
        #  inv_covar[1:4, 1:4] = np.linalg.inv(regularizer.get_params()['covariance'])
        #  w_analytic = np.dot(np.dot(np.linalg.inv(np.dot(np.transpose(train_x_with_ones), train_x_with_ones)
        #                             + inv_covar * linreg.get_params()['noise_variance']), np.transpose(train_x_with_ones)), train_y)
        #
        #  se = (train_y - np.dot(train_x_with_ones, w_analytic))**2
        #  print("analytic mse = ", np.mean(se))
        #  print("w primitive = {} - and w analytic = {}".format(w_primitive, w_analytic[1:4]))
        #  print(np.linalg.norm(w_primitive-w_analytic[1:4]))
        #
        #  self.assertTrue(linreg.get_call_metadata().iterations_done == 500)
        #  self.assertTrue(np.linalg.norm(w_primitive-w_analytic[1:4]) < 0.04)

    #  def test_noise_prior(self):
    #      """
    #      nothing yet exists to provide a prior for a float
    #      """
    #
    #  def test_batches(self):
    #      hy = Hyperparams.defaults()
    #      hy1 = dict(hy)
    #      hy1['alpha'] = 5e-3
    #      hy1['beta'] = 0
    #
    #      hy2 = Hyperparams(hy1)
    #
    #      linreg = LinearRegression(hyperparams=hy2)
    #      # train_x = np.array([[9, 8], [3, 2], [7, 2], [5, 4], [5, 5], [9, 3]])
    #      train_x = np.array([[3, 4], [-3, -2], [0, -2], [2, -3], [1, 1], [-3, 2]])
    #      weights = np.array([1, 0.4])
    #      train_y = np.dot(train_x, weights) + 0.000001*np.random.randn(len(train_x))
    #      linreg.set_training_data(inputs=train_x, outputs=train_y)
    #      linreg.fit(iterations=300, batch_size=3, fit_threshold=0)
    #      pred_weights = linreg.get_params()['weights']
    #      print(pred_weights)
    #      self.assertTrue(
    #          np.linalg.norm(weights-pred_weights)
    #          < 0.1)
    #
    #  def test_gradients(self):
    #      linreg = LinearRegression(hyperparams=Hyperparams.defaults())
    #      linreg.set_params(params=Params(weights=ndarray([1, 1], generate_metadata=True), offset=0, noise_variance=2))
    #      grad = linreg.gradient_output(outputs=np.array([3]), inputs=np.array([[1, 1]]))[0]
    #
    #      # https://www.wolframalpha.com/input/?i=d%2Fdy(-+(y-2)%5E2+)%2F(2*2)+at+y+%3D+3
    #      print(grad)
    #      expected_grad = -0.5
    #
    #      self.assertEqual(grad, expected_grad)
    #
    #      # next test grad_params similarly...
    #
    #  def test_sample(self):
    #      linreg = LinearRegression(hyperparams=Hyperparams.defaults())
    #      linreg.set_params(params=Params(weights=ndarray([1, 1], generate_metadata=True), offset=0, noise_variance=0.0001))
    #
    #      # Check second sample is within 50 SDs of the expected value.
    #      self.assertTrue(3.5 < linreg.sample(inputs=np.array([[3, 1]]), num_samples=2)[0][0] < 4.5)
    #
    #      # check second sample is within 50 SDs of the expected value
    #      assert 3.5 < linreg.sample(inputs=np.array([[3, 1]]), num_samples=2)[1][0] < 4.5
    #
    #  def test_produce(self):
    #      linreg = LinearRegression(hyperparams=Hyperparams.defaults())
    #      linreg.set_params(params=Params(weights=ndarray([1, 1], generate_metadata=True), offset=0, noise_variance=0.0001))
    #      p = linreg.produce(inputs=np.array([[3, 1], [5, 9]])).value
    #
    #      assert p[0] == 4
    #      assert p[1] == 14
    #
    #  def test_log_likelihood(self):
    #      linreg = LinearRegression(hyperparams=Hyperparams.defaults())
    #      linreg.set_params(params=Params(weights=ndarray([1, 1], generate_metadata=True), offset=0, noise_variance=0.0001))
    #      assert linreg.log_likelihood(outputs=np.array([2.1, 3.1]),
    #                                   inputs=np.array([[1, 1], [2, 1]])).value >\
    #          linreg.log_likelihood(outputs=[2.2, 3],
    #                                inputs=[np.array([1, 1]), np.array([2, 1])]).value


if __name__ == '__main__':
    unittest.main()
