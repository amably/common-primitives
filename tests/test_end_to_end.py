import unittest

from d3m.container.numpy import ndarray
import numpy as np  # type: ignore

from common_primitives import feed_forward_neural_net as ffnn, loss
from utils import get_mnist_train_data


class EndToEndTestCase(unittest.TestCase):
    @unittest.skip("Skipping end-to-end test - relies on impl. of ffnn which does not support it with metadata at the moment")
    def test_end_to_end(self):
        """
        End-to-end train three MLPs and test against expected resuts
        """
        # These particular values are picked because they result in a test that concludes quickly
        xor_inputs = ndarray([[0, 0], [0, 1], [1, 0], [1, 1]], generate_metadata=True)
        xor_outputs = ndarray([[0], [1], [1], [0]], generate_metadata=True)

        hy = ffnn.Hyperparams(ffnn.Hyperparams.defaults(), depth = 2, width = 4, input_dim = 2, output_dim = 4, activation_type = 'tanh')
        primitive1 = ffnn.FeedForwardNeuralNet(hyperparams=hy)

        hy = ffnn.Hyperparams(ffnn.Hyperparams.defaults(), depth = 1, width = 4, input_dim = 4, output_dim = 2, activation_type = 'tanh')
        primitive2 = ffnn.FeedForwardNeuralNet(hyperparams=hy)

        hy = ffnn.Hyperparams(ffnn.Hyperparams.defaults(), depth = 1, width = 4, input_dim = 2, output_dim = 1, activation_type = 'tanh')
        primitive3 = ffnn.FeedForwardNeuralNet(hyperparams=hy)

        # hy['target_inputs'] = xor_outputs
        hy = loss.Hyperparams(loss.Hyperparams.defaults(), loss_type = 'mse', target_inputs = xor_outputs)
        pipeline_loss = loss.Loss(hyperparams=hy)

        # print(p1._net)
        # print(p2._net)
        # print(p3._net)
        # print('End-to-end training starting')
        end_to_end_iterations = 5000
        for i in range(end_to_end_iterations):
            # Forward
            o1 = primitive1.forward(inputs=xor_inputs)
            o2 = primitive2.forward(inputs=o1)
            o3 = primitive3.forward(inputs=o2)

            # Error
            mse = pipeline_loss.forward(inputs=o3)
            # print(i, mse)

            # Backpropagation and fine tuning
            grad_loss_o3, _ = pipeline_loss.backward(gradient_outputs=np.ones([1]))


            grad_o3_o2,   _ = primitive3.backward(gradient_outputs=grad_loss_o3, fine_tune=True, fine_tune_learning_rate=0.01)
            grad_o2_o1,   _ = primitive2.backward(gradient_outputs=grad_o3_o2, fine_tune=True, fine_tune_learning_rate=0.01)
            _,            _ = primitive1.backward(gradient_outputs=grad_o2_o1, fine_tune=True, fine_tune_learning_rate=0.01)


            if mse[0] < 0.1:
                break

        # print('End-to-end training finished')
        self.assertTrue(True)
