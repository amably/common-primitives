import unittest
import os

from common_primitives.image_reader import ImageReader, Hyperparams


@unittest.skip
class ImageReaderTestCase(unittest.TestCase):
    def test_produce(self):
        ir = ImageReader(hyperparams=Hyperparams.defaults())
        test_dir = os.path.dirname(os.path.realpath(__file__)) + "/test_image/"

        data_2_1_28_28 = ir.produce(inputs=[test_dir + 'mnist_0_1.png', test_dir + 'mnist_0_1.png']).value
        data_2_3_32_32 = ir.produce(inputs=[test_dir + 'cifar10_bird_1.png', test_dir + 'cifar10_bird_2.png']).value

        assert((data_2_1_28_28.shape == (2,1,28,28) and (data_2_3_32_32.shape == (2,3,32,32))))

    def test_resize(self):
        ir = ImageReader(hyperparams=Hyperparams(Hyperparams.defaults(), resize_to=(10, 10)))
        test_dir = os.path.dirname(os.path.realpath(__file__)) + "/test_image/"

        data_2_1_10_10 = ir.produce(inputs=[test_dir + 'mnist_0_1.png', test_dir + 'mnist_0_1.png']).value
        data_2_3_10_10 = ir.produce(inputs=[test_dir + 'cifar10_bird_1.png', test_dir + 'cifar10_bird_2.png']).value

        assert((data_2_1_10_10.shape == (2,1,10,10) and (data_2_3_10_10.shape == (2,3,10,10))))

if __name__ == '__main__':
    unittest.main()
