import os
import typing

from d3m import container, utils as d3m_utils
from d3m.metadata import base as metadata_base
from d3m.metadata import hyperparams
from d3m.primitive_interfaces import base, transformer

import common_primitives

__all__ = ('ListToDataFramePrimitive',)

Inputs = container.List
Outputs = container.DataFrame


class Hyperparams(hyperparams.Hyperparams):
    pass


class ListToDataFramePrimitive(transformer.TransformerPrimitiveBase[Inputs, Outputs, Hyperparams]):
    """
    A primitive which converts a list into a pandas dataframe.
    """

    metadata = metadata_base.PrimitiveMetadata(
        {
            'id': 'dd4598cf-2384-438a-a264-f6c77185132b',
            'version': '0.1.0',
            'name': "List to DataFrame converter",
            'python_path': 'd3m.primitives.data_transformation.list_to_dataframe.Common',
            'source': {
                'name': common_primitives.__author__,
                'contact': 'mailto:mitar.commonprimitives@tnode.com',
                'uris': [
                    'https://gitlab.com/datadrivendiscovery/common-primitives/blob/master/common_primitives/list_to_dataframe.py',
                    'https://gitlab.com/datadrivendiscovery/common-primitives.git',
                ],
            },
            'installation': [{
                'type': metadata_base.PrimitiveInstallationType.PIP,
                'package_uri': 'git+https://gitlab.com/datadrivendiscovery/common-primitives.git@{git_commit}#egg=common_primitives'.format(
                    git_commit=d3m_utils.current_git_commit(os.path.dirname(__file__)),
                ),
            }],
            'algorithm_types': [
                metadata_base.PrimitiveAlgorithmType.DATA_CONVERSION,
            ],
            'primitive_family': metadata_base.PrimitiveFamily.DATA_TRANSFORMATION,
        },
    )

    def produce(self, *, inputs: Inputs, timeout: float = None, iterations: int = None) -> base.CallResult[Outputs]:
        return base.CallResult(container.DataFrame(inputs, generate_metadata=True))

    @classmethod
    def can_accept(cls, *, method_name: str, arguments: typing.Dict[str, typing.Union[metadata_base.Metadata, type]],
                   hyperparams: Hyperparams) -> typing.Optional[metadata_base.DataMetadata]:
        output_metadata = super().can_accept(method_name=method_name, arguments=arguments, hyperparams=hyperparams)

        # If structural types didn't match, don't bother.
        if output_metadata is None:
            return None

        if method_name != 'produce':
            return output_metadata

        if 'inputs' not in arguments:
            return output_metadata

        inputs_metadata = typing.cast(metadata_base.DataMetadata, arguments['inputs'])

        # Not supported by Pandas DataFrame.
        if 'dimension' in inputs_metadata.query((metadata_base.ALL_ELEMENTS, metadata_base.ALL_ELEMENTS)):
            raise ValueError("Must pass 2-d input")

        outputs_metadata = inputs_metadata.update((), {
            'structural_type': container.DataFrame,
        })

        # If input was a one-dimensional list, it is still converted to a 2D DataFrame with one column.
        if 'dimension' not in outputs_metadata.query((metadata_base.ALL_ELEMENTS,)):
            outputs_metadata = outputs_metadata.copy_to(outputs_metadata, (metadata_base.ALL_ELEMENTS,), (metadata_base.ALL_ELEMENTS, metadata_base.ALL_ELEMENTS))
            outputs_metadata = outputs_metadata.remove((metadata_base.ALL_ELEMENTS,))
            outputs_metadata = outputs_metadata.update((metadata_base.ALL_ELEMENTS,), {
                'dimension': {
                    'length': 1,
                },
            })

        outputs_metadata = outputs_metadata.set_table_metadata()

        outputs_metadata = outputs_metadata.update((metadata_base.ALL_ELEMENTS,), {
            'structural_type': metadata_base.NO_VALUE,
        })

        return outputs_metadata
