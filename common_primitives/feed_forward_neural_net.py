from typing import Any, Dict, List, Optional, Tuple, Type, Set
import os
from collections import OrderedDict
import time

import numpy as np  # type: ignore
import torch  # type: ignore
import torch.nn as nn  # type: ignore
import torch.optim as optim  # type: ignore

from d3m import container, exceptions, utils as d3m_utils
from d3m.base import utils as base_utils
from d3m.metadata import base as metadata_base, hyperparams, params
from d3m.primitive_interfaces.supervised_learning import SupervisedLearnerPrimitiveBase
from d3m.primitive_interfaces.base import CallResult, GradientCompositionalityMixin, Gradients, DockerContainer

import common_primitives
from .utils import to_variable

Inputs = container.DataFrame
Outputs = container.DataFrame


class Params(params.Params):
    state: Dict
    target_columns_metadata: Optional[List[OrderedDict]]


class Hyperparams(hyperparams.Hyperparams):
    loss_type = hyperparams.Enumeration[str](
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        values=['mse', 'crossentropy'],
        default='crossentropy',
        description='Type of loss used during training (fit).'
    )
    input_dim = hyperparams.Hyperparameter[int](
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        default=1,
        description='Dimensions of the input.'
    )
    output_dim = hyperparams.Hyperparameter[int](
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        default=1,
        description='Dimensions the output.'
    )
    depth = hyperparams.Hyperparameter[int](
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        default=2,
        description='Total number of layers, including the output layer.'
    )
    width = hyperparams.Hyperparameter[int](
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        default=128,
        description='Number of units in each layer, except the last (output) layer, which is always equal to the output dimensions.'
    )
    activation_type = hyperparams.Enumeration[str](
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        values=['linear', 'relu', 'tanh', 'sigmoid'],
        default='relu',
        description='Type of activation (non-linearity) following each layer excet the last one.'
    )
    last_activation_type = hyperparams.Enumeration[str](
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        values=['same_with_activation_type', 'linear', 'relu', 'tanh', 'sigmoid'],
        default='same_with_activation_type',
        description='Type of activation (non-linearity) following the last layer.'
    )
    optimizer_type = hyperparams.Enumeration[str](
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        values=['adam', 'sgd'],
        default='adam',
        description='Type of optimizer used during training (fit).'
    )
    minibatch_size = hyperparams.Hyperparameter[int](
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        default=64,
        description='Minibatch size used during training (fit).'
    )
    learning_rate = hyperparams.Hyperparameter[float](
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        default=0.00001,
        description='Learning rate used during training (fit).'
    )
    momentum = hyperparams.Hyperparameter[float](
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        default=0.9,
        description='Momentum used during training (fit), only for optimizer_type sgd.'
    )
    weight_decay = hyperparams.Hyperparameter[float](
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        default=0.00001,
        description='Weight decay (L2 regularization) used during training (fit).'
    )
    shuffle = hyperparams.Hyperparameter[bool](
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        default=True,
        description='Shuffle minibatches in each epoch of training (fit).'
    )
    fit_threshold = hyperparams.Hyperparameter[float](
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        default=1e-5,
        description='Threshold of loss value to early stop training (fit).'
    )
    use_inputs_columns = hyperparams.Set(
        elements=hyperparams.Hyperparameter[int](-1),
        default=(),
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description="A set of inputs column indices to force primitive to operate on. If any specified column cannot be used, it is skipped.",
    )
    exclude_inputs_columns = hyperparams.Set(
        elements=hyperparams.Hyperparameter[int](-1),
        default=(),
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description="A set of inputs column indices to not operate on. Applicable only if \"use_columns\" is not provided.",
    )
    use_outputs_columns = hyperparams.Set(
        elements=hyperparams.Hyperparameter[int](-1),
        default=(),
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description="A set of outputs column indices to force primitive to operate on. If any specified column cannot be used, it is skipped.",
    )
    exclude_outputs_columns = hyperparams.Set(
        elements=hyperparams.Hyperparameter[int](-1),
        default=(),
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description="A set of outputs column indices to not operate on. Applicable only if \"use_columns\" is not provided.",
    )
    return_result = hyperparams.Enumeration(
        values=['append', 'replace', 'new'],
        # Default value depends on the nature of the primitive.
        default='append',
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description="Should resulting columns be appended, should they replace original columns, or should only resulting columns be returned?",
    )
    add_index_columns = hyperparams.UniformBool(
        default=True,
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description="Also include primary index columns if input data has them. Applicable only if \"return_result\" is set to \"new\".",
    )


class FeedForwardNeuralNet(GradientCompositionalityMixin[Inputs, Outputs, Params, Hyperparams],
                           SupervisedLearnerPrimitiveBase[Inputs, Outputs, Params, Hyperparams]):
    """
    A feed-forward neural network primitive using PyTorch.
    It can be configured with input and output dimensions, number of layers (depth),
    and number of units in each layer except the last one (width).
    """

    __author__ = 'Oxford DARPA D3M Team, Atilim Gunes Baydin <gunes@robots.ox.ac.uk>'
    metadata = metadata_base.PrimitiveMetadata({
        'id': '1307cda3-ba46-4274-a742-cadec75da2ec',
        'version': '0.1.0',
        'name': 'Feed-forward neural network',
        'keywords': ['neural network', 'multi-layer Perceptron', 'deep learning'],
        'source': {
            'name': common_primitives.__author__,
            'contact': 'mailto:gunes@robots.ox.ac.uk',
            'uris': [
                'https://gitlab.com/datadrivendiscovery/common-primitives/blob/master/common_primitives/extract_columns_semantic_types.py',
                'https://gitlab.com/datadrivendiscovery/common-primitives.git',
            ],
        },
        'installation': [{
            'type': metadata_base.PrimitiveInstallationType.PIP,
            'package_uri': 'git+https://gitlab.com/datadrivendiscovery/common-primitives.git@{git_commit}#egg=common-primitives'.format(
                git_commit=d3m_utils.current_git_commit(os.path.dirname(__file__))
                )
        }],
        'python_path': 'd3m.primitives.regression.feed_forward_neural_net.TorchCommon',
        'algorithm_types': ['NEURAL_NETWORK_BACKPROPAGATION'],
        'primitive_family': 'REGRESSION',
    })

    def __init__(self, *,
                 hyperparams: Hyperparams,
                 random_seed: int = 0,
                 docker_containers: Dict[str, DockerContainer] = None) -> None:

        super().__init__(hyperparams=hyperparams, random_seed=random_seed, docker_containers=docker_containers)
        torch.manual_seed(random_seed)
        self._net = None  # type: Type[torch.nn.Module]
        loss_type = self.hyperparams['loss_type']
        if loss_type == 'mse':
            self._criterion = nn.MSELoss()
            self._last_activation_type = self.hyperparams['last_activation_type']
        elif loss_type == 'crossentropy':
            self._criterion = nn.CrossEntropyLoss()
            self._last_activation_type = 'linear'
        else:
            raise ValueError('Unsupported loss_type: {}. Available options: mse, crossentropy'.format(loss_type))
        self._input_dim = self.hyperparams['input_dim']
        self._output_dim = self.hyperparams['output_dim']
        self._depth = self.hyperparams['depth']
        self._width = self.hyperparams['width']
        self._activation_type = self.hyperparams['activation_type']
        self._optimizer_type = self.hyperparams['optimizer_type']
        self._minibatch_size = self.hyperparams['minibatch_size']
        self._learning_rate = self.hyperparams['learning_rate']
        self._momentum = self.hyperparams['momentum']
        self._weight_decay = self.hyperparams['weight_decay']
        self._shuffle = self.hyperparams['shuffle']
        self._fit_threshold = self.hyperparams['fit_threshold']
        self._num_classes = 2

        self._inputs = None  # type: Type[torch.autograd.Variable]
        self._outputs = None  # type: Type[torch.autograd.Variable]
        self._training_inputs = None  # type: Type[torch.autograd.Variable]
        self._training_outputs = None  # type: Type[torch.autograd.Variable]
        self._training_size = 0
        self._iterations_done = 0
        self._label_index = None  # type: Set[Any]
        self._has_finished = True
        self._target_columns_metadata: List[OrderedDict] = None
        self._init_net()

    def _init_net(self) -> None:
        class _Net(nn.Module):
            def __init__(self, input_dim: int, output_dim: int,
                         depth: int, width: int,
                         activation_type: str, last_activation_type: str) -> None:
                super().__init__()
                #  a = 1 / 0
                self._input_dim = input_dim
                self._output_dim = output_dim
                if activation_type == 'linear':
                    self._activation = lambda x: x
                elif activation_type == 'relu':
                    self._activation = nn.ReLU()
                elif activation_type == 'tanh':
                    self._activation = nn.Tanh()
                elif activation_type == 'sigmoid':
                    self._activation = nn.Sigmoid()
                else:
                    raise ValueError('Unsupported activation_type: {}. Available options: linear, relu, tanh, sigmoid'.format(activation_type))
                if last_activation_type == 'same_with_activation_type':
                    self._last_activation = self._activation
                elif last_activation_type == 'linear':
                    self._last_activation = lambda x: x
                elif last_activation_type == 'relu':
                    self._last_activation = nn.ReLU()
                elif last_activation_type == 'tanh':
                    self._last_activation = nn.Tanh()
                elif last_activation_type == 'sigmoid':
                    self._last_activation = nn.Sigmoid()
                else:
                    raise ValueError('Unsupported last_activation_type: {}. Available options: same_with_activation_type, linear, relu, tanh, sigmoid'.format(last_activation_type))
                self._layers = []  # type: List[Any]
                if depth == 1:
                    self._layers.append(nn.Linear(self._input_dim, self._output_dim))
                elif depth > 1:
                    self._layers.append(nn.Linear(self._input_dim, width))
                    for i in range(depth - 1):
                        if i == depth - 2:
                            self._layers.append(nn.Linear(width, self._output_dim))
                        else:
                            self._layers.append(nn.Linear(width, width))
                else:
                    raise ValueError('Depth must be greater than or equal to one.')
                for i in range(len(self._layers)):
                    nn.init.xavier_uniform(self._layers[i].weight, gain=nn.init.calculate_gain(activation_type))
                    self.add_module('_layers[{0}]'.format(i), self._layers[i])

            def forward(self, x: torch.Tensor) -> torch.Tensor:
                x = x.view(-1, self._input_dim)
                for i in range(len(self._layers)):
                    layer = self._layers[i]
                    x = layer(x)
                    if i == (len(self._layers) - 1):
                        x = self._last_activation(x)
                    else:
                        x = self._activation(x)
                return x

        self._net = _Net(self._input_dim, self._output_dim, self._depth, self._width, self._activation_type, self._last_activation_type)

    @classmethod
    def _get_target_columns_metadata(cls, outputs_metadata: metadata_base.DataMetadata) -> List[OrderedDict]:
        outputs_length = outputs_metadata.query((metadata_base.ALL_ELEMENTS,))['dimension']['length']

        target_columns_metadata: List[OrderedDict] = []
        for column_index in range(outputs_length):
            column_metadata = OrderedDict(outputs_metadata.query_column(column_index))

            # Update semantic types and prepare it for predicted targets.
            semantic_types = list(column_metadata.get('semantic_types', []))
            if 'https://metadata.datadrivendiscovery.org/types/PredictedTarget' not in semantic_types:
                semantic_types.append('https://metadata.datadrivendiscovery.org/types/PredictedTarget')
            semantic_types = [semantic_type for semantic_type in semantic_types if semantic_type != 'https://metadata.datadrivendiscovery.org/types/TrueTarget']
            column_metadata['semantic_types'] = semantic_types

            target_columns_metadata.append(column_metadata)

        return target_columns_metadata

    def _store_target_columns_metadata(self, outputs: Outputs) -> None:
        self._target_columns_metadata = self._get_target_columns_metadata(outputs.metadata)

    # TODO: check whether all elements in inputs and outputs sequences have the same shape
    def set_training_data(self, *, inputs: Inputs, outputs: Outputs) -> None:
        if len(inputs) != len(outputs):
            raise ValueError('Training data sequences "inputs" and "outputs" should have the same length.')
        inputs, _ = self._select_inputs_columns(inputs)
        outputs, _ = self._select_outputs_columns(outputs)

        self._store_target_columns_metadata(outputs)

        self._training_size = len(inputs)
        self._training_inputs = to_variable(inputs.values)

        try:
            self._training_outputs = to_variable(outputs.values)
        except Exception as e:
            # not given integers
            # TODO really dumb hack here
            label_to_int = {}

            counter = 0
            self._label_index = set(outputs.values.flatten())
            if isinstance(self._criterion, nn.CrossEntropyLoss):
                self._output_dim = len(self._label_index)
            for class_label in self._label_index:
                label_to_int[class_label] = counter
                counter += 1

            new_outs = []
            for value in outputs.values.flatten():
                new_outs.append(label_to_int[value])

            self._training_outputs = to_variable(np.array(new_outs))

        #  self._training_outputs = to_variable(outputs)
        if self._training_inputs.dim() == 1:
            self._training_inputs = self._training_inputs.unsqueeze(0)
        self._input_dim = len(self._training_inputs[0].view(-1))
        if isinstance(self._criterion, nn.CrossEntropyLoss):
            self._training_outputs = self._training_outputs.type(torch.LongTensor)
        else:
            if self._training_outputs.dim() == 1:
                self._training_outputs = self._training_outputs.unsqueeze(1)
            if self._output_dim == 1:
                self._output_dim = len(self._training_outputs[0].view(-1))
        self._init_net()

    def produce(self, *, inputs: Inputs, timeout: float = None, iterations: int = None) -> CallResult[Outputs]:
        if self._net is None:
            raise Exception('Neural network not initialized. You need to set training data so that the network structure can be defined.')
        # TODO why is iterations_done reset to 0, is this the proper behavior if continuefit mixin is implemented?
        self._iterations_done = 0
        self._has_finished = True
        self._net.eval()

        selected_inputs, columns_to_use = self._select_inputs_columns(inputs)
        self._inputs = to_variable(selected_inputs.values, requires_grad=True)
        self._outputs = self._net.forward(self._inputs)

        lookup = list(self._label_index)  # type: List[Any]

        if isinstance(self._criterion, nn.CrossEntropyLoss):
            # normalize outputs
            normalized = torch.softmax(self._outputs, dim=1)
            labels = [np.where(r == 1)[0][0] for r in normalized]

            predictions = [lookup[i] for i in labels]
        else:
            # TODO
            #  predictions = [lookup[0] for _ in range(self._training_outputs.shape[0])]
            predictions = self._outputs.data.numpy()

        output_columns = [self._wrap_predictions(predictions)]
        outputs = base_utils.combine_columns(inputs, columns_to_use, output_columns, return_result=self.hyperparams['return_result'], add_index_columns=self.hyperparams['add_index_columns'])

        return CallResult(outputs)

    def fit(self, *, timeout: float = None, iterations: int = None) -> CallResult[None]:
        # TODO document the fit function
        '''

        '''
        if self._training_inputs is None:
            raise Exception('Cannot fit when no training data is present.')

        if timeout is None:
            timeout = np.inf
        if iterations is None:
            iterations = 100

        if self._minibatch_size > self._training_size:
            self._minibatch_size = self._training_size

        if self._optimizer_type == 'adam':
            optimizer_instance = optim.Adam(self._net.parameters(), lr=self._learning_rate, weight_decay=self._weight_decay)
        elif self._optimizer_type == 'sgd':
            optimizer_instance = optim.SGD(self._net.parameters(), lr=self._learning_rate, momentum=self._momentum, weight_decay=self._weight_decay)
        else:
            raise ValueError('Unsupported optimizer_type: {}. Available options: adam, sgd'.format(self._optimizer_type))

        start = time.time()
        self._iterations_done = 0
        # We can always iterate more, even if not reasonable.
        self._has_finished = False
        self._net.train()
        while time.time() < start + timeout and self._iterations_done < iterations:
            self._iterations_done += 1

            if self._shuffle:
                permute = torch.randperm(self._training_size)
                self._training_inputs = self._training_inputs[permute]
                self._training_outputs = self._training_outputs[permute]

            epoch_loss = 0.
            iteration = 0
            for i in range(0, self._training_size, self._minibatch_size):
                i_end = min(i + self._minibatch_size, self._training_size)
                minibatch_inputs = self._training_inputs[i:i_end]
                minibatch_outputs = self._training_outputs[i:i_end]
                optimizer_instance.zero_grad()
                o = self._net(minibatch_inputs)
                loss = self._criterion(o, minibatch_outputs)
                loss.backward()
                optimizer_instance.step()

                # print('iteration: {}, loss: {}'.format(iteration, loss.item()))
                epoch_loss += loss.item()
                iteration += 1

            epoch_loss /= iteration
            # print('epoch loss: {}'.format(epoch_loss))
            if epoch_loss < self._fit_threshold:
                self._has_finished = True
                return CallResult(None)
        return CallResult(None)

    def get_params(self) -> Params:
        return self._get_params(self._net.state_dict())

    def _get_params(self, state_dict: dict) -> Params:
        s = {k: v.numpy() for k, v in state_dict.items()}
        return Params(
                state=s,
                target_columns_metadata=None,
                )

    def set_params(self, *, params: Params) -> None:
        state = self._net.state_dict()
        new_state = {k: torch.from_numpy(v) for k, v in params['state'].items()}
        state.update(new_state)
        self._net.load_state_dict(state)
        self._target_columns_metadata = params['target_columns_metadata']

    def backward(self, *, gradient_outputs: Gradients[Outputs], fine_tune: bool = False, fine_tune_learning_rate: float = 0.00001,
                 fine_tune_weight_decay: float = 0.00001) -> Tuple[Gradients[Inputs], Gradients[Params]]:  # type: ignore

        # TODO document the backward function and use mathematically precise language
        '''

        '''
        if self._inputs is None:
            raise Exception('Cannot call backpropagation before forward propagation. Call "produce" before "backprop".')
        else:
            # TODO the weight decay will be reset at every backward if this is instantiated here, should cache this maybe?
            optimizer_instance = optim.SGD(self._net.parameters(), lr=fine_tune_learning_rate, weight_decay=fine_tune_weight_decay)

            if self._inputs.grad is not None:
                self._inputs.grad.data.zero_()
            optimizer_instance.zero_grad()
            self._outputs.backward(gradient=to_variable(gradient_outputs))

            if fine_tune:
                # TODO add in the fit term termperature
                optimizer_instance.step()

            grad_inputs = self._inputs.grad
            # Includes both trainable parameters (which will have derivatives) and state variables such as running averages (which won't have derivatives)
            grad_params_state_dict = {k: v.clone().fill_(0) for k, v in self._net.state_dict().items()}
            # Includes only trainable parameters (which will have derivatives)
            named_parameters = {k: v.grad.data for k, v in self._net.named_parameters()}
            grad_params_state_dict = {k: named_parameters[k] if k in named_parameters else v for k, v in grad_params_state_dict.items()}
            grad_params = self._get_params(grad_params_state_dict)
            return grad_inputs, grad_params  # type: ignore

    def gradient_output(self, *, outputs: Outputs, inputs: Inputs) -> Gradients[Outputs]:
        raise NotImplementedError()

    def gradient_params(self, *, outputs: Outputs, inputs: Inputs) -> Gradients[Params]:
        raise NotImplementedError()

    def set_fit_term_temperature(self, *, temperature: float = 0) -> None:
        self._fit_term_temperature = temperature

    @classmethod
    def _update_predictions_metadata(cls, outputs: Optional[Outputs], target_columns_metadata: List[OrderedDict]) -> metadata_base.DataMetadata:
        outputs_metadata = metadata_base.DataMetadata()
        if outputs is not None:
            outputs_metadata = outputs_metadata.generate(outputs)

        for column_index, column_metadata in enumerate(target_columns_metadata):
            outputs_metadata = outputs_metadata.update_column(column_index, column_metadata)

        return outputs_metadata

    def _wrap_predictions(self, predictions: np.ndarray) -> Outputs:
        outputs = container.DataFrame(predictions, generate_metadata=False)
        outputs.metadata = self._update_predictions_metadata(outputs, self._target_columns_metadata)
        return outputs

    @classmethod
    def _can_use_inputs_column(cls, inputs_metadata: metadata_base.DataMetadata, column_index: int) -> bool:
        column_metadata = inputs_metadata.query((metadata_base.ALL_ELEMENTS, column_index))

        if not d3m_utils.is_numeric(column_metadata['structural_type']):
            return False

        return 'https://metadata.datadrivendiscovery.org/types/Attribute' in column_metadata.get('semantic_types', [])

    @classmethod
    def _get_inputs_columns(cls, inputs_metadata: metadata_base.DataMetadata, hyperparams: Hyperparams) -> List[int]:
        def can_use_column(column_index: int) -> bool:

            return cls._can_use_inputs_column(inputs_metadata, column_index)

        columns_to_use, columns_not_to_use = base_utils.get_columns_to_use(inputs_metadata,  hyperparams['use_inputs_columns'], hyperparams['exclude_inputs_columns'], can_use_column)

        if not columns_to_use:
            raise ValueError("No inputs columns.")

        if hyperparams['use_inputs_columns'] and columns_not_to_use:
            cls.logger.warning("Not all specified inputs columns can used. Skipping columns: %(columns)s", {
                'columns': columns_not_to_use,
            })

        return columns_to_use

    @classmethod
    def _can_use_outputs_column(cls, outputs_metadata: metadata_base.DataMetadata, column_index: int) -> bool:
        column_metadata = outputs_metadata.query((metadata_base.ALL_ELEMENTS, column_index))

        return 'https://metadata.datadrivendiscovery.org/types/TrueTarget' in column_metadata.get('semantic_types', [])

    @classmethod
    def _get_outputs_columns(cls, outputs_metadata: metadata_base.DataMetadata, hyperparams: Hyperparams) -> List[int]:
        def can_use_column(column_index: int) -> bool:

            return cls._can_use_outputs_column(outputs_metadata, column_index)

        columns_to_use, columns_not_to_use = base_utils.get_columns_to_use(outputs_metadata, hyperparams['use_outputs_columns'], hyperparams['exclude_outputs_columns'], can_use_column)

        if not columns_to_use:
            raise ValueError("No outputs columns.")

        if hyperparams['use_outputs_columns'] and columns_not_to_use:
            cls.logger.warning("Not all specified outputs columns can used. Skipping columns: %(columns)s", {
                'columns': columns_not_to_use,
            })

        return columns_to_use

    def _select_inputs_columns(self, inputs: Inputs) -> Tuple[Inputs, List[int]]:
        columns_to_use = self._get_inputs_columns(inputs.metadata, self.hyperparams)

        return inputs.select_columns(columns_to_use), columns_to_use

    def _select_outputs_columns(self, outputs: Outputs) -> Tuple[Outputs, List[int]]:
        columns_to_use = self._get_outputs_columns(outputs.metadata, self.hyperparams)

        return outputs.select_columns(columns_to_use), columns_to_use
